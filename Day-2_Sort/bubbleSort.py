


def myBubbleSort(string=''):
    index = 0
    number = []
    inputString = []

    if string != '':
        inputString = string
    else:
        demoFile = open("Day-2_Sort/text.txt", "r")
        inputString = demoFile.read()

    while len(inputString) > index:
        if inputString[index] == "-":
            nbrLength = 1
            nbr = ""
            while inputString[index + nbrLength] != ' ' and inputString[index + nbrLength] != '\n':
                nbr = nbr + inputString[index + nbrLength]
                nbrLength += 1

            if nbr != '':
                number.append(int('-' + nbr))
            index = index + nbrLength
        elif inputString[index] != " " and inputString[1] != "\n":
            nbrLength = 0
            nbr = ""
            while inputString[index + nbrLength] != ' ' and inputString[index + nbrLength] != '\n':
                nbr = nbr + inputString[index + nbrLength]
                nbrLength = nbrLength + 1
            if nbr != '':
                number.append(int(nbr))
            index += nbrLength
        index += 1
    numberLength = len(number) - 1
    while 0 < numberLength:
        for i in range(numberLength):
            if number[i] > number[i + 1]:
                last = number[i]
                number[i] = number[i + 1]
                number[i + 1] = last
        numberLength += -1

    print(number)


def mySplit(entry, separator):
    return entry.split(separator)
