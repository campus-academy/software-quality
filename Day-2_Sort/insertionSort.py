demoFile = open("Day-2_Sort/text.txt", "r")

readDemoFile = demoFile.read()
index = 0
number = []

while len(readDemoFile) > index:
    if readDemoFile[index] == "-":
        nbrLength = 1
        nbr = ""
        while readDemoFile[index + nbrLength] != ' ' and readDemoFile[index + nbrLength] != '\n':
            nbr = nbr + readDemoFile[index + nbrLength]
            nbrLength += 1

        if nbr != '':
            number.append(int('-' + nbr))
        index = index + nbrLength
    elif readDemoFile[index] != " " and readDemoFile[1] != "\n":
        nbrLength = 0
        nbr = ""
        while readDemoFile[index + nbrLength] != ' ' and readDemoFile[index + nbrLength] != '\n':
            nbr = nbr + readDemoFile[index + nbrLength]
            nbrLength = nbrLength + 1
        if nbr != '':
            number.append(int(nbr))
        index += nbrLength
    index += 1

for i in range(1, len(number)):
    print(i)