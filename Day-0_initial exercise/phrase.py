# L'utilisateur entre sa phrase, on stock la phrase dans la variable 'text'
text = input('Entrez votre phrase')
# on utilise la fonction split qui découpe la chaine, et stock les mots dans un tableau, on stock le résultat
# dans la variable splitted_text
splitted_text = text.split()
# on affiche le résultat
print('La phrase : ' + text + 'a été découpé mot par mot ce qui donne')
print(splitted_text)
