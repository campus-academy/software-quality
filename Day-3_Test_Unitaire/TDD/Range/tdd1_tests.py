import unittest
import LeTrucADevelopper as ldd


class TestTDD1Methods(unittest.TestCase):
    def test_return_type_for_one_param(self):
        self.assertEqual(type(ldd.le_truc_a_developper(10)), type([]))

    def test_return_type_for_two_param(self):
        self.assertEqual(type(ldd.le_truc_a_developper(1, 10)), type([]))

    def test_return_type_for_three_param(self):
        self.assertEqual(type(ldd.le_truc_a_developper(1, 10, 2)), type([]))

    def test_return_for_one_param(self):
        number = 10
        self.assertEqual(len(ldd.le_truc_a_developper(number)), number)
        self.assertEqual(ldd.le_truc_a_developper(number)[0], 0)

        precedentNumber = 0
        for i in ldd.le_truc_a_developper(number):
            self.assertEqual(i, precedentNumber + 1)
            precedentNumber = i



    def test_return_for_two_params(self):
        start = 5
        end = 10
        self.assertEqual(len(ldd.le_truc_a_developper(start, end)), end - start)
        self.assertEqual(ldd.le_truc_a_developper(start, end)[0], start)
        self.assertEqual(ldd.le_truc_a_developper(start, end)[-1], end - 1)

        precedentNumber = start
        for i in ldd.le_truc_a_developper(start, end):
            self.assertEqual(i, precedentNumber + 1)
            precedentNumber = i

    def test_return_for_three_params(self):
        start = 1
        end = 11
        step = 2
        self.assertEqual(len(ldd.le_truc_a_developper(start, end, step)), round((end - start) / step))
        self.assertEqual(ldd.le_truc_a_developper(start, end, step)[0], start)

        precedentNumber = start
        for i in ldd.le_truc_a_developper(start, end, step):
            self.assertEqual(i, precedentNumber + step)
            precedentNumber = i


if __name__ == '__main__':
    unittest.main()