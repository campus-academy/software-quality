def one():
    return 'Denutrition'


def two():
    return "Maigreur"


def three(imc):
    return [imc, "Corpulence Normale"]


def four(imc):
    return [imc, "Surpoids"]


def five(imc):
    return [imc, "Obesite moderee"]


def six(imc):
    return [imc, "Obesite severe"]


def seven(imc):
    return [imc, "Obesite morbide"]


switcher = {
        1: one,
        2: two,
        3: three,
        4: four,
        5: five,
        6: six,
        7: seven
    }


def tempete_du_desert(poids, taille):
    result = poids / (taille * taille)
    return result


def imc_to_label(imc):
    func = switcher.get(imc, "nothing")
    return func


print(imc_to_label(1))

print(tempete_du_desert(61, 1.80))
