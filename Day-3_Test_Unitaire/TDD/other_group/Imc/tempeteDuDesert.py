def tempete_du_desert(poids, taille):
    result = poids / (taille * taille)

    if result <= 16.4:
        return result, 'Denutrition'
    elif result <= 18.5:
        return result, 'Maigreur'
    elif result <= 25:
        return result, 'Corpulence normale'
    elif result <= 30:
        return result, 'Surpoids'
    elif result <= 35:
        return result, 'Obesite modere'
    elif result <= 40:
        return result, 'Obesite severe'
    elif result >= 41:
        return result, 'Obesite morbide'

    return 'Aucune donnee liee a cet IMC: ', result


