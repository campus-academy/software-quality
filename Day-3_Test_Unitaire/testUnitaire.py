import unittest
import sys
sys.path.append('/Users/swann/Dev/Python/Day-2_Sort/')
import bubbleSort


class TestStringMethods(unittest.TestCase):
    def test_isEqual(self):
        a = "1+3"
        b = ["1", "3"]
        c = bubbleSort.mySplit(a, '+')
        self.assertEqual(b, c)

    def test_isDouble(self):
        a = "1++++++++3"
        b = ["1", "3"]
        c = bubbleSort.mySplit(a, '+')
        self.assertEqual(b, c)

    def test_entryType(self):
        a = "1+3"
        b = "13"
        self.assertEqual(type(a), type(b))

    def test_resultType(self):
        a = "1+3"
        b = ["1", "3"]
        c = bubbleSort.mySplit(a, '+')
        self.assertEqual(type(c), type(b))

    def test_acceptNegativeInt(self):
        a = "-1+-3"
        b = ["-1", "-3"]
        c = bubbleSort.mySplit(a, '+')
        self.assertEqual(c, b)


if __name__ == '__main__':
    unittest.main()
